<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Tickets extends Controller {

    public function action_index() {
        
    }

    public function action_buy() {
        $session = isset($_POST['session']) ? (int) $_POST['session'] : null;
        $places = isset($_POST['places']) ? explode(',', ($_POST['places'])) : null;
        if (is_null($session) || is_null($places)) {
            $result = array();
        } else {
            $ApiModel = new Model_Api();
            $result = $ApiModel->buyTickets($session, $places);
        }
        $this->response->headers('content-type', 'application/json; charset=' . Kohana::$charset);
        $this->response->body(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function action_reject() {
        $uniqKey = isset($_POST['key']) ? $_POST['key'] : null;
        $ApiModel = new Model_Api();
        $result = $ApiModel->rejectTicket($uniqKey);
        $this->response->headers('content-type', 'application/json; charset=' . Kohana::$charset);
        $this->response->body(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

}

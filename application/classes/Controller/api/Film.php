<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Film extends Controller {

    public function action_index() {
        
    }

    public function action_schedule() {
        $film = $this->request->param('id');
        $ApiModel = new Model_Api();
        $result = $ApiModel->getFilmSchedule($film);
        $this->response->headers('content-type', 'application/json; charset=' . Kohana::$charset);
        $this->response->body(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Cinema extends Controller {

    public function action_index() {
        
    }

    public function action_schedule() {
        $cinema = $this->request->param('id');
        $hall = isset($_GET['hall']) ? (int) $_GET['hall'] : '%';
        $ApiModel = new Model_Api();
        $result = $ApiModel->getCinemaSchedule($cinema, $hall);
        $this->response->headers('content-type', 'application/json; charset=' . Kohana::$charset);
        $this->response->body(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

}

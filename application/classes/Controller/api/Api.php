<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Api extends Controller {

    public function action_index() {
        $arr = array('api_version' => 'v1');
        $this->response->headers('content-type', 'application/json; charset=' . Kohana::$charset);
        $this->response->body(json_encode($arr));
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Api extends Kohana_Model {

    public function getCinemaSchedule($CinemaName, $idhall) {
        $query = DB::query(Database::SELECT, 'SELECT cinema.`name` as cinema,films_name.`name` as filmsName,
                                                halls.`name` as hall, sessions.time as time
                                              FROM sessions
                                              INNER JOIN halls ON halls.id = sessions.hall
                                              INNER JOIN cinema ON halls.cinema = cinema.id
                                              INNER JOIN films_name ON sessions.`name` = films_name.id
                                              WHERE cinema.`name` = :cinema
                                              AND halls.id LIKE :idhall');
        $query->param(':cinema', $CinemaName);
        $query->param(':idhall', $idhall);
        $schedule_arr = array();
        $result = $query->execute()->as_array();
        foreach ($result as $key => $value) {
            $schedule_arr[$value['cinema']][$value['hall']][$value['filmsName']][] = date('d-m-Y H:i:s ', strtotime($value['time']));
        }
        return $schedule_arr;
    }

    public function getFilmSchedule($film) {
        $query = DB::query(Database::SELECT, 'SELECT cinema.`name` as cinema,films_name.`name` as filmsName,
                                                halls.`name` as hall, sessions.time as time
                                              FROM sessions
                                              INNER JOIN halls ON halls.id = sessions.hall
                                              INNER JOIN cinema ON halls.cinema = cinema.id
                                              INNER JOIN films_name ON sessions.`name` = films_name.id
                                              WHERE films_name.name = :film');
        $query->param(':film', $film);
        $result = $query->execute()->as_array();
        foreach ($result as $key => $value) {
            $schedule_arr[$value['filmsName']][$value['cinema']][$value['hall']][] = date('d-m-Y H:i:s ', strtotime($value['time']));
        }
        return $schedule_arr;
    }

    public function getSessionPlace($session) {
        $query = DB::query(Database::SELECT, 'SELECT cinema.`name` as cinema,
                                                     halls.`name` as hall,
                                                     films_name.`name` as film,
                                                     places.`row`,
                                                     places.place,
                                                     places.reserv
                                              FROM sessions
                                              INNER JOIN halls ON halls.id = sessions.hall
                                              INNER JOIN cinema ON halls.cinema = cinema.id
                                              INNER JOIN films_name ON sessions.`name` = films_name.id
                                              INNER JOIN places ON places.hall = sessions.hall
                                              WHERE sessions.id = :session
                                              AND places.reserv=0');
        $query->param(':session', $session);
        $result = $query->execute()->as_array();
        foreach ($result as $key => $value) {
            $schedule_arr[$value['cinema']][$value['hall']][$value['film']][$value['row']][] = $value['place'];
        }
        return $schedule_arr;
    }

    public function buyTickets($session, $places) {
        $query = DB::query(Database::INSERT, "INSERT INTO `tickets` (`session`, `place`, `uniq_id`) VALUES (:session, :place, :uniqid)");
        $query->param(':session', $session);
        foreach ($places as $place) {
            $query->param(':place', $place);
            $query->param(':uniqid', uniqid());
            $result[] = $query->execute();
        }
        return $result;
    }

    public function rejectTicket($uniqKey) {
        $query = DB::query(Database::DELETE, 'DELETE FROM `tickets` WHERE (`uniq_id`=:uniqKey)');
        $query->param(':uniqKey', $uniqKey);
        return $result = $query->execute();
    }

}

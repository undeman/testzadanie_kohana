/*
Navicat MySQL Data Transfer

Source Server         : Denwer
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : tz

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2014-04-12 19:31:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cinema`
-- ----------------------------
DROP TABLE IF EXISTS `cinema`;
CREATE TABLE `cinema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cinema
-- ----------------------------
INSERT INTO `cinema` VALUES ('1', 'Харбин');
INSERT INTO `cinema` VALUES ('2', 'Россия');

-- ----------------------------
-- Table structure for `films_name`
-- ----------------------------
DROP TABLE IF EXISTS `films_name`;
CREATE TABLE `films_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of films_name
-- ----------------------------
INSERT INTO `films_name` VALUES ('1', 'Ной');
INSERT INTO `films_name` VALUES ('2', 'Рио 2');
INSERT INTO `films_name` VALUES ('3', 'Первый мститель: Другая война');
INSERT INTO `films_name` VALUES ('4', 'Окулус');
INSERT INTO `films_name` VALUES ('5', '3 дня на убийство ');

-- ----------------------------
-- Table structure for `films_time`
-- ----------------------------
DROP TABLE IF EXISTS `films_time`;
CREATE TABLE `films_time` (
  `id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of films_time
-- ----------------------------

-- ----------------------------
-- Table structure for `halls`
-- ----------------------------
DROP TABLE IF EXISTS `halls`;
CREATE TABLE `halls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `cinema` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of halls
-- ----------------------------
INSERT INTO `halls` VALUES ('1', 'Малый зал', '1', '5');
INSERT INTO `halls` VALUES ('2', 'Малый зал', '2', '3');
INSERT INTO `halls` VALUES ('3', 'Большой зал', '2', '1');

-- ----------------------------
-- Table structure for `places`
-- ----------------------------
DROP TABLE IF EXISTS `places`;
CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hall` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `reserv` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of places
-- ----------------------------
INSERT INTO `places` VALUES ('1', '2', '1', '1', '0');
INSERT INTO `places` VALUES ('2', '2', '1', '2', '0');
INSERT INTO `places` VALUES ('3', '2', '1', '3', '0');
INSERT INTO `places` VALUES ('4', '2', '1', '4', '0');
INSERT INTO `places` VALUES ('5', '3', '1', '1', '0');

-- ----------------------------
-- Table structure for `sessions`
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `hall` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('1', '3', '1', '2014-04-10 21:30:07');
INSERT INTO `sessions` VALUES ('2', '1', '2', '2014-04-10 21:30:11');
INSERT INTO `sessions` VALUES ('3', '2', '3', '2014-04-08 23:48:27');
INSERT INTO `sessions` VALUES ('4', '3', '3', '0000-00-00 00:00:00');
INSERT INTO `sessions` VALUES ('5', '2', '3', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `tickets`
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `uniq_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tickets
-- ----------------------------
INSERT INTO `tickets` VALUES ('33', '3', '1', '5349078e6129b');
INSERT INTO `tickets` VALUES ('34', '3', '3', '5349078f65e4e');
INSERT INTO `tickets` VALUES ('35', '3', '5', '5349078f68d42');
INSERT INTO `tickets` VALUES ('36', '3', '7', '5349078f6bc1a');
